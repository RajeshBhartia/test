import React from "react";
import ReactDOM from "react-dom";
import AppTest from "./App";

ReactDOM.render(<AppTest />, document.getElementById("root"));
