import React, { Component } from 'react';
import SnackBar from './Snackbar';
import "./styles/App.scss"
class App extends Component {
  componentDidMount() {
    this.snackBarRef.onOpen("default snackbar");
  }
  render() {
    
    return (
      <div className="app">
        Hello
        <SnackBar 
          ref={r => this.snackBarRef = r}
          />
      </div>
    );
  }
}

export default App;