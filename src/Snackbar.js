/**
 * Author: uttam, created: 11-02-2020
 * Description: Class component for showing SnackBar
 * Mandatory props: No mandatory props
 * Optional args: snackBarPos
 * Test file : PROJECT_ROOT/src/sgf/testings/snackBar/SnackBar.test.js
 */

/**
 * @methods
      this.snackBarRef.onOpen("default snackbar");
      this.snackBarRef.onOpen("dismiss snackbar after 6 s", 6000);
      this.snackBarRef.onOpen("dismiss snackbar after calling onClose() method", -1);
      this.snackBarRef.onClose();
 */

import React, { Component } from "react";
import { MDCSnackbar } from "@material/snackbar";
import PropTypes from "prop-types";
import Close from "./resources/close.svg";
import "./styles/Snackbar.scss";

class SnackBar extends Component {
  state = {
    notificationMsg: ""
  }

  componentDidMount() {
    new MDCSnackbar(
      document.querySelector(".mdc-snackbar")
    );
  }

  onOpen = (notificationMsg, timeout = 5000) => {
    let visibleTime = 5000;
    if ((timeout >= 4000 && timeout <= 10000) || timeout === -1) {
      visibleTime = timeout;
    }
    
    this.setState({
      notificationMsg
    }, () => {
      const mdcSnackbar = new MDCSnackbar(
        document.querySelector(".mdc-snackbar")
      );
      mdcSnackbar.timeoutMs = visibleTime
      mdcSnackbar.open();
    })
  }

  onClose = () => {
    const mdcSnackbar = new MDCSnackbar(
      document.querySelector(".mdc-snackbar")
    );
    mdcSnackbar.close("");
  }

  render() {
    let pos = this.props.snackBarPos ? this.props.snackBarPos : "baseline"
    return (
      <div>
        <div
          className={`mdc-snackbar mdc-snackbar--${pos} fw-snackbar-wrapper`}
        >
          <div className="mdc-snackbar__surface">
            <div
              className="mdc-snackbar__label"
              role="status"
              aria-live="polite"
            >
              {this.state.notificationMsg}
            </div>
            <div className="mdc-snackbar__actions">
              <button
                onClick={this.onClose}
                className="mdc-icon-button material-icons mdc-snackbar__action custom-dialog-close-icon">
                <img src={Close} alt="Cancel" />
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SnackBar;

SnackBar.propTypes = {
  snackBarPos: PropTypes.oneOf(["baseline", "leading"])
};
